#include "changedata.h"
#include "ui_changedata.h"
#include "admin.h"
#include "QMessageBox"
#include "QTimer"

changedata::changedata(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::changedata)
{
    Client = new QTcpSocket;
//    Client->connectToHost("127.0.0.1", 33333);
//    connect(Client, SIGNAL(readyRead()), SLOT(slotReadyReady()));
//    Client->write("Database");
//    slotReadyReady();
    ui->setupUi(this);
    QPixmap bkgnd("../Laba1/222.jpg");
        bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
        QPalette palette;
        palette.setBrush(QPalette::Background, bkgnd);
        this->setPalette(palette);



}


changedata::~changedata()
{
    delete ui;
}

void changedata::on_BAT2_clicked()
{
    QMessageBox msgBox;
    QString ID  = ui->ID->text();
    QString Tip  = ui->NAME->text();
    QString Post  = ui->TEL->text();
    QString To4ka = ui->DATA->text();
    QString Vvod = ui->lineEdit->text();
    QString Vsvod = ui->lineEdit_2->text();

    int a0 = ID.length();
    int a1 = Tip.length();
    int a2 = Post.length();
    int a3 = To4ka.length();
    int a4 = Vvod.length();
    int a5 = Vsvod.length();

    QByteArray temp;
    if (a0 > 0 and a1 > 0 and a2 > 0 and a3 > 0 and a4 > 0 and a5 > 0 ){
        QString tempo = "changedb$" + ID + "@" + Tip + "&"  + Post + "#" + To4ka + "*"  + Vvod + "?" + Vsvod + "!";
        temp.append(tempo);
        Client->write(temp);
    }
    else if (a0 > 0 and a1 > 0) {
        QString tempo = "changedb$" + ID + "@" + Tip + "&"  ;
        temp.append(tempo);
        Client->write(temp);

    }
    else if (a0 > 0 and a2 > 0) {
        QString tempo = "changedb$" + ID + "@" + "&"  + Post + "#" ;
        temp.append(tempo);
        Client->write(temp);

    }
    else if (a0 > 0 and a3 > 0) {
        QString tempo = "changedb$" + ID + "@"  + "#" + To4ka + "*" ;
        temp.append(tempo);
        Client->write(temp);

    }
    else if (a0 > 0 and a4 > 0) {
        QString tempo = "changedb$" + ID + "@"+ "*" + Vvod + "?" ;
        temp.append(tempo);
        Client->write(temp);

    }
    else if (a0 > 0 and a5 > 0) {
        QString tempo = "changedb$" + ID + "@"  + "?"  + Vsvod + "!" ;
        temp.append(tempo);
        Client->write(temp);

    }
    msgBox.setText("Успешно изменено!");
    msgBox.exec();
    Client->write("Database");
    ui->ID->setText("");
    ui->NAME->setText("");
    ui->TEL->setText("");
    ui->DATA->setText("");
    ui->lineEdit->setText("");
    ui->lineEdit_2->setText("");

}

void changedata::on_pushButton_clicked()
{
    hide();
    Admin window1;
    qintptr id = Client->socketDescriptor();
    connect(this, SIGNAL(connectSocket(qintptr)), &window1, SLOT(slot_connectSocket(qintptr)));
    emit connectSocket(id);
    window1.setModal(true);
    window1.exec();
}

void changedata::slotReadyReady() {

    QString message, tempstr, tempstr1;
    int start, middle, end, count, status12,supa;
    dataS tempstruc;


    while(Client->bytesAvailable())
    {
    QString buffer = Client->readAll();
    message.append(buffer);
    }

     if(message.indexOf("@") >=0)
    {

        count = message.mid(0, message.indexOf("@")).toInt();

        for(int i = 0; i < count; i++)
        {

            start = message.indexOf("@");

            middle = message.indexOf("&");
            status12 = message.indexOf("*");
            end = message.indexOf("?");
            supa = message.indexOf("^");

            tempstr = message.mid((start + 1), (end - start));

            tempstruc.log = tempstr.mid(0, tempstr.indexOf("&"));
            tempstruc.pas = tempstr.mid(tempstr.indexOf("&") + 1, tempstr.indexOf("*") - tempstr.indexOf("&") - 1);
            tempstruc.status = tempstr.mid(tempstr.indexOf("*") + 1, tempstr.indexOf("?") - tempstr.indexOf("*") - 1);
            tempstruc.test1 = message.mid(message.indexOf("?") + 1, message.indexOf("!") - message.indexOf("?") - 1 );
            tempstruc.test2 = message.mid(message.indexOf("!") + 1, message.indexOf("^") - message.indexOf("!") - 1);


            Vikar.push_back(tempstruc);

            tempstr1 = message.mid((supa + 1), message.size());
            message.clear();
            message = tempstr1;


            QStringList list = {"Тип", "Поставщик", "Торговая Точка", "Ввоз", "Вывоз"};
             BDmodel = new QStandardItemModel(Vikar.size(),4,this);
             ui->tableView->setModel(BDmodel);
             QModelIndex index;
             BDmodel->QStandardItemModel::setHorizontalHeaderLabels(list);
             ui->tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
             ui->tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
             ui->tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);


                QString value;
                    for(int row =0;row< BDmodel->rowCount();++row)
                    {
                        for(int col =0;col< BDmodel->columnCount();++col)
                        {
                            index = BDmodel->index(row,col);


                            if(col == 0)
                            {
                                value = Vikar[row].log;
                                BDmodel->setData(index,value);
                            }

                            else if(col == 1)
                            {
                                value = Vikar[row].pas;
                                BDmodel->setData(index,value);
                            }
                            else if (col == 2) {
                                value = Vikar[row].status;
                                BDmodel->setData(index,value);
                            }
                            else if (col == 3 ) {
                                value = Vikar[row].test1;
                                BDmodel->setData(index,value);
                            }
                            else if (col == 4 ) {
                                value = Vikar[row].test2;
                                BDmodel->setData(index,value);
                            }



                        }
                    }
                    for(int row =0;row< BDmodel->rowCount();++row)
                    {
                        ui->tableView->verticalHeader()->setSectionResizeMode(row, QHeaderView::Stretch);
                    }



        }
        Vikar.clear();


    }

}

void changedata::slot_connectSocket(qintptr id)
{
    Client->setSocketDescriptor(id);
    connect(Client, SIGNAL(readyRead()), SLOT(slotReadyReady()));
    Client->write("Database");
    slotReadyReady();
}
