#ifndef FIND_H
#define FIND_H

#include <QDialog>
#include "QStandardItemModel"
#include <QTcpSocket>
#include "accs.h"

namespace Ui {
class Find;
}

class Find : public QDialog
{
    Q_OBJECT
signals:
    void connectSocket(qintptr id);
public:
    explicit Find(QWidget *parent = nullptr);
    ~Find();

private slots:
    void on_pushButton_2_clicked();
    bool sort(dataS obg, int ck1,int ck2, int ck3, int ck4, int ck5);
    void on_pushButton_clicked();
    void slotReadyReady();
    void resirvedText(QString str);
    void slot_connectSocket(qintptr id);
    void on_pushButton_3_clicked();

private:
    Ui::Find *ui;
    QStandardItemModel *BDmodel;
    vector<dataS> Vikar;
    QTcpSocket * Client;
    vector<dataS> sort_db;
};

#endif // FIND_H

