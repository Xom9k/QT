#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"
#include <cstring>
#include <string>
#include <fstream>
#include <iostream>
#include "admin.h"
#include "reg.h"
#include "space.h"
#include <QCryptographicHash>
using namespace std;



MainWindow::MainWindow(QWidget *parent) :  QMainWindow(parent), ui(new Ui::MainWindow)
{
    Client = new QTcpSocket;
    Client->connectToHost("127.0.0.1", 33333);
    connect(Client, SIGNAL(readyRead()), SLOT(slotReadyReady()));
    ui->setupUi(this);
    QPixmap bkgnd("../Laba1/asd.jpg");
        bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
        QPalette palette;
        palette.setBrush(QPalette::Background, bkgnd);
        this->setPalette(palette);

}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::slotReadyReady(){

    QMessageBox msgBox;
    QString message;



    while(Client->bytesAvailable())
    {
    QString buffer = Client->readAll();
    message.append(buffer);
    }

    if (message == "admin")
       {
            hide();
            Space window;
            qintptr id = Client->socketDescriptor();
            connect(this, SIGNAL(connectSocket(qintptr)), &window, SLOT(slot_connectSocket(qintptr)));
            emit connectSocket(id);
            msgBox.setText("Добро пожаловать, Админ!");
            msgBox.exec();
            window.setModal(true);
            window.exec();
            ui->vvodLog->setText("");
            ui->vvodPass->setText("");
            show();


       }
       else if(message == "user")
       {
//           hide();

//           Find window;
//           window.setModal(true);
//           window.exec();
           sndwindow = new Find();
           qintptr id = Client->socketDescriptor();
           connect(this, SIGNAL(connectSocket(qintptr)), sndwindow, SLOT(slot_connectSocket(qintptr)));
           emit connectSocket(id);
           connect(this, SIGNAL(sendText(QString)),sndwindow,SLOT(resirvedText(QString)));
           sndwindow->show();
           emit sendText("user");
           ui->vvodLog->setText("");
           ui->vvodPass->setText("");
//            close();

       }

       else if(message == "manager")
       {
//          hide();
//          Manager window;
//          window.setModal(true);
//          window.exec();
        sndwindoww = new Admin();
        qintptr id = Client->socketDescriptor();
        connect(this, SIGNAL(connectSocket(qintptr)), sndwindoww, SLOT(slot_connectSocket(qintptr)));
        emit connectSocket(id);
        connect(this, SIGNAL(sendText(QString)),sndwindoww,SLOT(resirvedText(QString)));
        sndwindoww->show();
        emit sendText("manager");
          ui->vvodLog->setText("");
          ui->vvodPass->setText("");
//          show();    sndwindoww = new Admin();
       }

       else {

           msgBox.setText("Login is incorrect");
           msgBox.setInformativeText("Do you want to retry?");
           msgBox.setStandardButtons(QMessageBox::Retry | QMessageBox::Close);
           msgBox.setDefaultButton(QMessageBox :: Retry);
           ui->vvodLog->setText("");
           ui->vvodPass->setText("");

       }

}


void MainWindow::slotSendToServer(){
    QString login  = ui->vvodLog->text();
    QString pas  = ui->vvodPass->text();
    QByteArray temp, cryp;
    cryp =  pas.toUtf8();
    cryp =  QCryptographicHash::hash(cryp, QCryptographicHash::Sha256).toHex();
    QString tempo = "log_in$" + login + "@" + cryp + "&";

    temp.append(tempo);

    unsigned char *key = (unsigned char *) "11111111111111111111111111111111";
    unsigned char *iv = (unsigned char *)   "0123456789012345";
    int crypLen;//

      char out_buf[256] = { 0 };
//      unsigned char testtt[256] = { 0 };
       unsigned char tuta[256] = { 0 };
//       const std::size_t count = temp.size();
//       unsigned char* hex = new unsigned char[count];
//       std::memcpy(hex,temp.constData(),count);
        EVP_CIPHER_CTX *ctx;
        QByteArray ba = tempo.toLocal8Bit();
          const char *ert = ba.data();
          int t = ba.size();

      ctx = EVP_CIPHER_CTX_new();
      EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv);
      int len = 0;
      EVP_EncryptUpdate(ctx,(unsigned char *)out_buf,&len, (unsigned char *)ert, t);
      crypLen = len;
      EVP_EncryptFinal_ex(ctx, (unsigned char *)out_buf + len, &len);
      crypLen += len;

      QByteArray papa;
         papa = QByteArray((char*)out_buf, crypLen);



    Client->write(papa);
}


void MainWindow::on_Batton_clicked()
{
    Client->close();
    Client = new QTcpSocket;
    Client->connectToHost("127.0.0.1", 33333);
    connect(Client, SIGNAL(readyRead()), SLOT(slotReadyReady()));
    slotSendToServer();
    slotReadyReady();
}


void MainWindow::on_pushButton_clicked()
{
    hide();
    reg  window;
    qintptr id = Client->socketDescriptor();
    connect(this, SIGNAL(connectSocket(qintptr)), &window, SLOT(slot_connectSocket(qintptr)));
    emit connectSocket(id);
    window.setModal(true);
    window.exec();
    show();
}




