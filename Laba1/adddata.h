#ifndef ADDDATA_H
#define ADDDATA_H

#include <QDialog>
#include "QStandardItemModel"
#include <QTcpSocket>
#include "accs.h"

namespace Ui {
class AddData;
}

class AddData : public QDialog
{
    Q_OBJECT

public:
    explicit AddData(QWidget *parent = nullptr);
    ~AddData();

signals:
    void connectSocket(qintptr id);
private slots:
    void on_Batton1_clicked();
     void slot_connectSocket(qintptr id);

    void on_pushButton_clicked();
    void slotReadyReady();

private:
    Ui::AddData *ui;
    QStandardItemModel *BDmodel;
    vector<dataS> Vikar;
    QTcpSocket * Client;
};


#endif


