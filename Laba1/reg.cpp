#include "reg.h"
#include "ui_reg.h"
#include "QMessageBox"
#include <QCryptographicHash>
#include "mainwindow.h"

reg::reg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::reg)
{
    Client = new QTcpSocket;
//    Client->connectToHost("127.0.0.1", 33333);
//     connect(Client, SIGNAL(readyRead()), SLOT(slotReadyReady()));
    ui->setupUi(this);
    QPixmap bkgnd("../Laba1/333.jpeg");
        bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
        QPalette palette;
        palette.setBrush(QPalette::Background, bkgnd);
        this->setPalette(palette);
}

reg::~reg()
{
    delete ui;

}


void reg::on_pushButton_clicked()
{
      slotSendToServery();
      slotReadyReady();
}


void reg::slotReadyReady(){
    QString message;
    QMessageBox msgBox;


    while(Client->bytesAvailable())
    {
    QString buffer = Client->readAll();
    message.append(buffer);
    }
    if (message == "zbs") {
        msgBox.setText("Вы зарегестрировыны!");
        msgBox.exec();}
    if (message == "no") {
        msgBox.setText("Они заняты, попробуйте другие!");
        msgBox.exec();}
}



void reg::slotSendToServery() {
    QString login  = ui->log->text();
    QString pas  = ui->pass->text();
    QString status = "user";
    QByteArray temp, cryp;
    cryp =  pas.toUtf8();
    cryp =  QCryptographicHash::hash(cryp, QCryptographicHash::Sha256).toHex();

    QString tempo = "reg$" + login + "@" + cryp + "&" + status + "#";
    temp.append(tempo);
    unsigned char *key = (unsigned char *) "11111111111111111111111111111111";
    unsigned char *iv = (unsigned char *)   "0123456789012345";
    int crypLen;//

      char out_buf[256] = { 0 };
//      unsigned char testtt[256] = { 0 };
       unsigned char tuta[256] = { 0 };
//       const std::size_t count = temp.size();
//       unsigned char* hex = new unsigned char[count];
//       std::memcpy(hex,temp.constData(),count);
        EVP_CIPHER_CTX *ctx;
        QByteArray ba = tempo.toLocal8Bit();
          const char *ert = ba.data();
          int t = ba.size();

      ctx = EVP_CIPHER_CTX_new();
      EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv);
      int len = 0;
      EVP_EncryptUpdate(ctx,(unsigned char *)out_buf,&len, (unsigned char *)ert, t);
      crypLen = len;
      EVP_EncryptFinal_ex(ctx, (unsigned char *)out_buf + len, &len);
      crypLen += len;

      QByteArray papa;
         papa = QByteArray((char*)out_buf, crypLen);



    Client->write(papa);
}

void reg::slot_connectSocket(qintptr id)
{
    Client->setSocketDescriptor(id);
    connect(Client, SIGNAL(readyRead()), SLOT(slotReadyReady()));
}

