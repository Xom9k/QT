#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include "find.h"
#include "admin.h"
#include <openssl/conf.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/aes.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void connectSocket(qintptr id);
    void sendText(QString str);

private slots:
    void on_Batton_clicked();
    void slotReadyReady();
    void on_pushButton_clicked();
    void slotSendToServer();
private:
    Ui::MainWindow *ui;
    QTcpSocket * Client ;
    Find *sndwindow;
    Admin *sndwindoww;

};



#endif // MAINWINDOW_H

