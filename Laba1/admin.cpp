#include "admin.h"
#include "ui_admin.h"
#include "adddata.h"
#include "changedata.h"
#include "deldata.h"
#include "find.h"
#include "space.h"
#include "QMessageBox"

Admin::Admin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Admin)
{
    Client = new QTcpSocket;
//    Client->connectToHost("127.0.0.1", 33333);
//    connect(Client, SIGNAL(readyRead()), SLOT(slotReadyReady()));
    ui->setupUi(this);
    QPixmap bkgnd("../Laba1/123.jpg");
        bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
        QPalette palette;
        palette.setBrush(QPalette::Background, bkgnd);
        this->setPalette(palette);


}

Admin::~Admin()
{
    delete ui;
}


void Admin::on_Batton1_clicked()
{
    close();
    AddData window;
    qintptr id = Client->socketDescriptor();
    connect(this, SIGNAL(connectSocket(qintptr)), &window, SLOT(slot_connectSocket(qintptr)));
    emit connectSocket(id);
    window.setModal(true);
    window.exec();
    show();

}

void Admin::on_Batton2_clicked()
{
    close();
    changedata window;
    qintptr id = Client->socketDescriptor();
    connect(this, SIGNAL(connectSocket(qintptr)), &window, SLOT(slot_connectSocket(qintptr)));
    emit connectSocket(id);
    window.setModal(true);
    window.exec();

    show();

}

void Admin::on_Batton3_clicked()
{
    close();
    DelData window;
    qintptr id = Client->socketDescriptor();
    connect(this, SIGNAL(connectSocket(qintptr)), &window, SLOT(slot_connectSocket(qintptr)));
    emit connectSocket(id);
    window.setModal(true);
    window.exec();
    show();

}




void Admin::on_pushButton_clicked()
{
    close();
    Find window;
    qintptr id = Client->socketDescriptor();
    connect(this, SIGNAL(connectSocket(qintptr)), &window, SLOT(slot_connectSocket(qintptr)));
    emit connectSocket(id);
    window.setModal(true);
    window.exec();
    show();

}

void Admin::on_pushButton_2_clicked()
{
    if (ui->labelSt->text() == "manager"){
        QMessageBox msg;
        msg.setText("You don't have access to this function!");
        msg.exec();
    }
    else {
        close();
        Space window1;
        Space window;
        qintptr id = Client->socketDescriptor();
        connect(this, SIGNAL(connectSocket(qintptr)), &window, SLOT(slot_connectSocket(qintptr)));
        emit connectSocket(id);
        window.setModal(true);
        window.exec();
        window1.setModal(true);
        window1.exec();
    }
}

void Admin::slotReadyReady() {

    QString message, tempstr, tempstr1;
    int start, middle, end, count, status12, supa;
    dataS tempstruc;


    while(Client->bytesAvailable())
    {
    QString buffer = Client->readAll();
    message.append(buffer);
    }

     if(message.indexOf("@") >=0)
    {

        count = message.mid(0, message.indexOf("@")).toInt();

        for(int i = 0; i < count; i++)
        {

            start = message.indexOf("@");

            middle = message.indexOf("&");
            status12 = message.indexOf("*");
            end = message.indexOf("?");
            supa = message.indexOf("^");

            tempstr = message.mid((start + 1), (end - start));

            tempstruc.log = tempstr.mid(0, tempstr.indexOf("&"));
            tempstruc.pas = tempstr.mid(tempstr.indexOf("&") + 1, tempstr.indexOf("*") - tempstr.indexOf("&") - 1);
            tempstruc.status = tempstr.mid(tempstr.indexOf("*") + 1, tempstr.indexOf("?") - tempstr.indexOf("*") - 1);
            tempstruc.test1 = message.mid(message.indexOf("?") + 1, message.indexOf("!") - message.indexOf("?") - 1 );
            tempstruc.test2 = message.mid(message.indexOf("!") + 1, message.indexOf("^") - message.indexOf("!") - 1);


            Vika.push_back(tempstruc);

            tempstr1 = message.mid((supa + 1), message.size());
            message.clear();
            message = tempstr1;



            QStringList list = {"Тип", "Поставщик", "Торговая Точка", "Ввоз", "Вывоз"};
             BDmodel = new QStandardItemModel(Vika.size(),4,this);
             ui->tableView->setModel(BDmodel);
             QModelIndex index;
             BDmodel->QStandardItemModel::setHorizontalHeaderLabels(list);
             ui->tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
             ui->tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
             ui->tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);


                QString value;
                    for(int row =0;row< BDmodel->rowCount();++row)
                    {
                        for(int col =0;col< BDmodel->columnCount();++col)
                        {
                            index = BDmodel->index(row,col);


                            if(col == 0)
                            {
                                value = Vika[row].log;
                                BDmodel->setData(index,value);

                            }

                            else if(col == 1)
                            {
                                value = Vika[row].pas;
                                BDmodel->setData(index,value);
                            }
                            else if (col == 2) {
                                value = Vika[row].status;
                                BDmodel->setData(index,value);
                            }
                            else if (col == 3 ) {
                                value = Vika[row].test1;
                                BDmodel->setData(index,value);
                            }
                            else if (col == 4 ) {
                                value = Vika[row].test2;
                                BDmodel->setData(index,value);
                            }



                        }
                    }
                    for(int row =0;row< BDmodel->rowCount();++row)
                    {
                        ui->tableView->verticalHeader()->setSectionResizeMode(row, QHeaderView::Stretch);
                    }


        }
        Vika.clear();


    }

}
void Admin::resirvedText(QString str)
{
    ui->labelSt->setText(str);
}


void Admin::slot_connectSocket(qintptr id)
{
    Client->setSocketDescriptor(id);
    connect(Client, SIGNAL(readyRead()), SLOT(slotReadyReady()));
    Client->write("Database");
    slotReadyReady();
}

