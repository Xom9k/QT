#ifndef SPACE_H
#define SPACE_H

#include <QDialog>
#include <QTcpSocket>
namespace Ui {
class Space;
}

class Space : public QDialog
{
    Q_OBJECT

public:
    explicit Space(QWidget *parent = nullptr);
    ~Space();

signals:
    void connectSocket(qintptr id);

private slots:
    void on_pushButton_clicked();
    void slot_connectSocket(qintptr id);
    void on_pushButton_2_clicked();

private:
    Ui::Space *ui;
    QTcpSocket * Client;
};

#endif // SPACE_H
