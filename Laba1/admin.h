#ifndef ADMIN_H
#define ADMIN_H

#include <QDialog>
#include "QStandardItemModel"
#include <QTcpSocket>
#include "accs.h"



namespace Ui {
class Admin;
}

class Admin : public QDialog
{
    Q_OBJECT

public:
    explicit Admin(QWidget *parent = nullptr);
    ~Admin();

public slots:

signals:
    void connectSocket(qintptr id);

private slots:

        void on_Batton1_clicked();

        void on_Batton2_clicked();
        void slot_connectSocket(qintptr id);
        void on_Batton3_clicked();

        void resirvedText(QString str);
        void on_pushButton_clicked();

        void on_pushButton_2_clicked();
        void  slotReadyReady();

private:
    Ui::Admin *ui;
    QStandardItemModel *BDmodel;
    QTcpSocket * Client;
    vector<dataS> Vika;
};

#endif // ADMIN_H


