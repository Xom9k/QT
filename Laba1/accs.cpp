#include "accs.h"
#include "ui_accs.h"
#include "space.h"
#include "QMessageBox"
#include <QCryptographicHash>

Accs::Accs(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Accs)
{
    ui->setupUi(this);
    Client = new QTcpSocket;
//    Client->connectToHost("127.0.0.1", 33333);
//    connect(Client, SIGNAL(readyRead()), SLOT(slotReadyReady()));
//    Client->write("Accounts");
//    slotReadyReady();
}

Accs::~Accs()
{
    delete ui;
}

void Accs::on_pushButton_clicked()
{
    hide();
    Space window;
    qintptr id = Client->socketDescriptor();
    connect(this, SIGNAL(connectSocket(qintptr)), &window, SLOT(slot_connectSocket(qintptr)));
    emit connectSocket(id);
    window.setModal(true);
    window.exec();
}

void Accs::on_pushButton_2_clicked()
{
    QMessageBox msgBox;
    QString login_f  = ui->Log_f->text();
//    QString pas_f = ui->Pas_f->text();
    QString login_t  = ui->Log_t->text();
    QString pas_t  = ui->Pas_t->text();
    QString status_t  = ui->Sta_t->text();
    int a1 = login_t.length();
    int a2 = pas_t.length();
    int a3 = status_t.length();
    QByteArray temp,cryp;
    if ((a2 > 0) and (a1 > 0 ) and (a3 > 0)) {
        cryp =  pas_t.toUtf8();
        cryp =  QCryptographicHash::hash(cryp, QCryptographicHash::Sha256).toHex();
        QString tempo = "changeacc$" + login_f + "@" + login_t + "&" + cryp + "#" + status_t + "*";
        temp.append(tempo);
        Client->write(temp);
        slotReadyReady();
    }
    else if ((a2 > 0) and (a3 > 0)) {
        cryp =  pas_t.toUtf8();
        cryp =  QCryptographicHash::hash(cryp, QCryptographicHash::Sha256).toHex();
        QString tempo = "changeacc$" + login_f + "@" + "&" + cryp + "#" + status_t + "*";
        temp.append(tempo);
        Client->write(temp);
    }
    else if ( (a1 > 0 ) and (a3 > 0)) {
        QString tempo = "changeacc$" + login_f + "@" +  login_t + "&" + "#" + status_t + "*";
        temp.append(tempo);
        Client->write(temp);
    }
    else if ( (a1 > 0 ) and (a2 > 0)) {
        cryp =  pas_t.toUtf8();
        cryp =  QCryptographicHash::hash(cryp, QCryptographicHash::Sha256).toHex();
        QString tempo = "changeacc$" + login_f + "@" +  login_t + "&"  + cryp + "#";
        temp.append(tempo);
        Client->write(temp);
    }
    else if ( a2 > 0 ) {
        cryp =  pas_t.toUtf8();
        cryp =  QCryptographicHash::hash(cryp, QCryptographicHash::Sha256).toHex();
         QString tempo = "changeacc$" + login_f + "@" + "&" + cryp + "#";
         temp.append(tempo);
         Client->write(temp);
    }
    else if (  (a3 > 0)) {
         QString tempo = "changeacc$" + login_f + "@" + "#" + status_t + "*";
         temp.append(tempo);
         Client->write(temp);
    }
    else if ((a1 > 0 )) {
    QString tempo = "changeacc$" + login_f + "@" + login_t + "&";
    temp.append(tempo);
    Client->write(temp);
    }
    msgBox.setText("Изменение успешно");
    msgBox.exec();
    Client->write("Accounts");

}

void Accs::on_pushButton_5_clicked()
{
    QMessageBox msgBox;
    QString login_f  = ui->Log_f->text();
//    QString login_t  = ui->Log_t->text();
//    QString pas_t  = ui->Pas_t->text();
//    QString status_t  = ui->Sta_t->text();
    QByteArray temp;


    QString tempo = "delacc$" + login_f + "@" + "login" + "&";
    temp.append(tempo);
    Client->write(temp);
    msgBox.setText("Изменение успешно");
    msgBox.exec();
    Client->write("Accounts");

}

void Accs::slotReadyReady() {

    QString message, tempstr, tempstr1;
    int start, middle, end, count, status12;
    dataS tempstruc;


    while(Client->bytesAvailable())
    {
    QString buffer = Client->readAll();
    message.append(buffer);
    }

     if(message.indexOf("@") >=0)
    {

        count = message.mid(0, message.indexOf("@")).toInt();

        for(int i = 0; i < count; i++)
        {

            start = message.indexOf("@");

            middle = message.indexOf("&");
            status12 = message.indexOf("*");
            end = message.indexOf("?");

            tempstr = message.mid((start + 1), (end - start));

            tempstruc.log = tempstr.mid(0, tempstr.indexOf("&"));
            tempstruc.pas = tempstr.mid(tempstr.indexOf("&") + 1, tempstr.indexOf("*") - tempstr.indexOf("&") - 1);
            tempstruc.status = tempstr.mid(tempstr.indexOf("*") + 1, tempstr.indexOf("?") - tempstr.indexOf("*") - 1);


            Vika.push_back(tempstruc);

            tempstr1 = message.mid((end + 1), message.size());
            message.clear();
            message = tempstr1;



            QStringList list = {"Login", "Password(hash)", "Status"};

            BDmodel = new QStandardItemModel(Vika.size(),2,this);


                ui->tableView->setModel(BDmodel);

                BDmodel->setHorizontalHeaderLabels(list);

                QModelIndex index;
                QString value;
                    for(int row =0;row< BDmodel->rowCount();++row)
                    {
                        for(int col =0;col< BDmodel->columnCount();++col)
                        {
                            index = BDmodel->index(row,col);

                            if(col == 0)
                            {
                                value = Vika[row].log;
                                BDmodel->setData(index,value);
                            }

                            else if(col == 1)
                            {
                            value = Vika[row].pas;
                            BDmodel->setData(index,value);
                            }
                            else if (col == 2) {
                                value = Vika[row].status;
                                BDmodel->setData(index,value);
                            }



                        }
                    }


        }
        Vika.clear();


    }

}

void Accs::slot_connectSocket(qintptr id)
{
    Client->setSocketDescriptor(id);
    connect(Client, SIGNAL(readyRead()), SLOT(slotReadyReady()));
    Client->write("Accounts");
    slotReadyReady();
}
