#include "adddata.h"
#include "ui_adddata.h"
#include "admin.h"
#include "QMessageBox"


AddData::AddData(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddData)
{
    Client = new QTcpSocket;
//    Client->connectToHost("127.0.0.1", 33333);
//    connect(Client, SIGNAL(readyRead()), SLOT(slotReadyReady()));
//    Client->write("Database");
//    slotReadyReady();
    ui->setupUi(this);
    QPixmap bkgnd("../Laba1/111.jpg");
        bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
        QPalette palette;
        palette.setBrush(QPalette::Background, bkgnd);
        this->setPalette(palette);
}

AddData::~AddData()
{
    delete ui;
}



void AddData::on_Batton1_clicked()
{
    QMessageBox msgBox;
    QString Tip  = ui->Name->text();
    QString Post  = ui->Tel->text();
    QString To4ka = ui->Data->text();
    QString Vvod = ui->VVod->text();
    QString Vsvod = ui->Vsvod->text();
    QByteArray temp;

    QString tempo = "addbd$" + Tip + "@" + Post  + "&" + To4ka + "#" + Vvod + "*" + Vsvod + "?" ;
    temp.append(tempo);
    Client->write(temp);
    msgBox.setText("Успешное добавление!");
    msgBox.exec();
    Client->write("Database");
    ui->Name->setText("");
    ui->Tel->setText("");
    ui->Data->setText("");
    ui->VVod->setText("");
    ui->Vsvod->setText("");
}

void AddData::on_pushButton_clicked()
{
    hide();
    Admin window1;
    qintptr id = Client->socketDescriptor();
    connect(this, SIGNAL(connectSocket(qintptr)), &window1, SLOT(slot_connectSocket(qintptr)));
    emit connectSocket(id);
    window1.setModal(true);
    window1.exec();
}

void AddData::slotReadyReady() {

    QString message, tempstr, tempstr1;
    int start, middle, end, count, status12,supa;
    dataS tempstruc;


    while(Client->bytesAvailable())
    {
    QString buffer = Client->readAll();
    message.append(buffer);
    }

     if(message.indexOf("@") >=0)
    {

        count = message.mid(0, message.indexOf("@")).toInt();

        for(int i = 0; i < count; i++)
        {

            start = message.indexOf("@");

            middle = message.indexOf("&");
            status12 = message.indexOf("*");
            end = message.indexOf("?");
            supa = message.indexOf("^");

            tempstr = message.mid((start + 1), (end - start));

            tempstruc.log = tempstr.mid(0, tempstr.indexOf("&"));
            tempstruc.pas = tempstr.mid(tempstr.indexOf("&") + 1, tempstr.indexOf("*") - tempstr.indexOf("&") - 1);
            tempstruc.status = tempstr.mid(tempstr.indexOf("*") + 1, tempstr.indexOf("?") - tempstr.indexOf("*") - 1);
            tempstruc.test1 = message.mid(message.indexOf("?") + 1, message.indexOf("!") - message.indexOf("?") - 1 );
            tempstruc.test2 = message.mid(message.indexOf("!") + 1, message.indexOf("^") - message.indexOf("!") - 1);


            Vikar.push_back(tempstruc);

            tempstr1 = message.mid((supa + 1), message.size());
            message.clear();
            message = tempstr1;


            QStringList list = {"Тип", "Поставщик", "Торговая Точка", "Ввоз", "Вывоз"};
             BDmodel = new QStandardItemModel(Vikar.size(),4,this);
             ui->tableView->setModel(BDmodel);
             QModelIndex index;
             BDmodel->QStandardItemModel::setHorizontalHeaderLabels(list);
             ui->tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
             ui->tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
             ui->tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);


                QString value;
                    for(int row =0;row< BDmodel->rowCount();++row)
                    {
                        for(int col =0;col< BDmodel->columnCount();++col)
                        {
                            index = BDmodel->index(row,col);


                            if(col == 0)
                            {
                                value = Vikar[row].log;
                                BDmodel->setData(index,value);
                            }

                            else if(col == 1)
                            {
                                value = Vikar[row].pas;
                                BDmodel->setData(index,value);
                            }
                            else if (col == 2) {
                                value = Vikar[row].status;
                                BDmodel->setData(index,value);
                            }
                            else if (col == 3 ) {
                                value = Vikar[row].test1;
                                BDmodel->setData(index,value);
                            }
                            else if (col == 4 ) {
                                value = Vikar[row].test2;
                                BDmodel->setData(index,value);
                            }



                        }
                    }
                    for(int row =0;row< BDmodel->rowCount();++row)
                    {
                        ui->tableView->verticalHeader()->setSectionResizeMode(row, QHeaderView::Stretch);
                    }



        }
        Vikar.clear();


    }

}

void AddData::slot_connectSocket(qintptr id)
{
    Client->setSocketDescriptor(id);
    connect(Client, SIGNAL(readyRead()), SLOT(slotReadyReady()));
    Client->write("Database");
    slotReadyReady();
}
