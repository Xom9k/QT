#ifndef REG_H
#define REG_H

#include <QDialog>
#include <QMainWindow>
#include <QTcpSocket>

namespace Ui {
class reg;
}

class reg : public QDialog
{
    Q_OBJECT

public:
    explicit reg(QWidget *parent = nullptr);
    ~reg();

signals:
    void connectSocket(qintptr id);

private slots:
    void on_pushButton_clicked();
    void slotReadyReady();
    void slotSendToServery();
    void slot_connectSocket(qintptr id);

    void on_pushButton_2_clicked();

private:
    Ui::reg *ui;
    QTcpSocket * Client;
};

#endif // REG_H

