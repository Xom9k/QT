#include "find.h"
#include "ui_find.h"
#include "admin.h"
#include "QMessageBox"
#include "mainwindow.h"
#include "ui_mainwindow.h"

Find::Find(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Find)
{
    Client = new QTcpSocket;
//    Client->connectToHost("127.0.0.1", 33333);
//    connect(Client, SIGNAL(readyRead()), SLOT(slotReadyReady()));
//    Client->write("Database");
//    slotReadyReady();
    ui->setupUi(this);




    }



Find::~Find()
{
    delete ui;
    Client->close();
}

void Find::on_pushButton_2_clicked()
{
    if (ui->labelSt->text() == "user"){
        QMessageBox msg;
        msg.setText("You don't have access to this function!");
        msg.exec();
    }
    else {
        hide();
        Admin window1;
        qintptr id = Client->socketDescriptor();
        connect(this, SIGNAL(connectSocket(qintptr)), &window1, SLOT(slot_connectSocket(qintptr)));
        emit connectSocket(id);
        window1.setModal(true);
        window1.exec();
    }
}

void Find::on_pushButton_clicked()
{
    QString Tip  = ui->FIO->text();
    QString Post  = ui->TEL->text();
    QString To4ka = ui->DATA->text();
    QString Vvod = ui->lineEdit->text();
    QString Vsvod = ui->lineEdit_2->text();
    dataS test;


    test.log = Tip;
    test.pas = Post;
    test.status = To4ka;
    test.test1 = Vvod;
    test.test2 = Vsvod;

    int a1 = Tip.length();
    int a2 = Post.length();
    int a3 = To4ka.length();
    int a4 = Vvod.length();
    int a5 = Vsvod.length();


    if (sort(test, a1, a2, a3, a4, a5) == 1)

    {

          BDmodel = new QStandardItemModel(sort_db.size(),3,this);
          ui->tableView->setModel(BDmodel);
          QStringList list = {"Тип", "Поставщик", "Торг. Точка", "Ввоз", "Вывоз"};
          BDmodel->QStandardItemModel::setHorizontalHeaderLabels(list);

          QModelIndex index;



          for (int col = 0; col < BDmodel->columnCount(); col++)
          {
              int n = 0;
              for (int row = 0; row < BDmodel->rowCount(); row++)
              {
                  index = BDmodel->index(row, col);
                   QString value;
                  if (col == 0)
                  {
                        value = sort_db[row].log;
                        BDmodel->setData(index, value);
                  }
                  else if (col == 1)
                  {
                       value = sort_db[row].pas;
                      BDmodel->setData(index, value);

                  }
                  else if (col == 2)
                  {
                       value = sort_db[row].status;
                      BDmodel->setData(index, value);

                  }
                  else if (col == 3)
                  {
                      value = sort_db[row].test1;
                     BDmodel->setData(index, value);
                  }
                  else if (col == 4)
                  {
                      value = sort_db[row].test2;
                     BDmodel->setData(index, value);
                  }


                  n++;
              }

           }




}
Vikar.clear();
sort_db.clear();
}

void Find::slotReadyReady() {

    QString message, tempstr, tempstr1;
    int start, middle, end, count, status12,supa;
    dataS tempstruc;


    while(Client->bytesAvailable())
    {
    QString buffer = Client->readAll();
    message.append(buffer);
    }

     if(message.indexOf("@") >=0)
    {

        count = message.mid(0, message.indexOf("@")).toInt();

        for(int i = 0; i < count; i++)
        {
            start = message.indexOf("@");

            middle = message.indexOf("&");
            status12 = message.indexOf("*");
            end = message.indexOf("?");
            supa = message.indexOf("^");

            tempstr = message.mid((start + 1), (end - start));

            tempstruc.log = tempstr.mid(0, tempstr.indexOf("&"));
            tempstruc.pas = tempstr.mid(tempstr.indexOf("&") + 1, tempstr.indexOf("*") - tempstr.indexOf("&") - 1);
            tempstruc.status = tempstr.mid(tempstr.indexOf("*") + 1, tempstr.indexOf("?") - tempstr.indexOf("*") - 1);
            tempstruc.test1 = message.mid(message.indexOf("?") + 1, message.indexOf("!") - message.indexOf("?") - 1 );
            tempstruc.test2 = message.mid(message.indexOf("!") + 1, message.indexOf("^") - message.indexOf("!") - 1);


            Vikar.push_back(tempstruc);

            tempstr1 = message.mid((supa + 1), message.size());
            message.clear();
            message = tempstr1;



            QStringList list = {"Тип", "Поставщик", "Торговая Точка", "Ввоз", "Вывоз"};
             BDmodel = new QStandardItemModel(Vikar.size(),4,this);
             ui->tableView->setModel(BDmodel);
             QModelIndex index;
             BDmodel->QStandardItemModel::setHorizontalHeaderLabels(list);
             ui->tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
             ui->tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
             ui->tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);


                QString value;
                    for(int row =0;row< BDmodel->rowCount();++row)
                    {

                        for(int col =0;col< BDmodel->columnCount();++col)
                        {
                            index = BDmodel->index(row,col);


                            if(col == 0)
                            {
                                value = Vikar[row].log;
                                BDmodel->setData(index,value);
                            }

                            else if(col == 1)
                            {
                                value = Vikar[row].pas;
                                BDmodel->setData(index,value);
                            }
                            else if (col == 2) {
                                value = Vikar[row].status;
                                BDmodel->setData(index,value);
                            }
                            else if (col == 3 ) {
                                value = Vikar[row].test1;
                                BDmodel->setData(index,value);
                            }
                            else if (col == 4 ) {
                                value = Vikar[row].test2;
                                BDmodel->setData(index,value);
                            }



                        }
                    }
                    for(int row =0;row< BDmodel->rowCount();++row)
                    {
                        ui->tableView->verticalHeader()->setSectionResizeMode(row, QHeaderView::Stretch);
                    }


        }



    }

}


bool Find::sort(dataS obg, int ck1,int ck2, int ck3,int ck4,int ck5)
      {
          int answ = 0;

{
              int k = Vikar.size();
              dataS obl;

              for  (int i = 0; i < k; i++)
              {
                 int stat1 = 0;
              if (ck1 > 1)
              {
                  stat1++;

                  if (obg.log == Vikar[i].log)
                  {

                       stat1 --;
                  }
               }

                   if (ck2 > 1)
                  {
                      stat1++;
                      if (obg.pas == Vikar[i].pas)
                      {
                          stat1 --;
                      }
                   }

                   if (ck3 > 1)
                      {
                          stat1++;
                          if (obg.status == Vikar[i].status)
                          {
                              stat1 --;
                          }
                      }
                   if (ck4 > 1)
                      {
                          stat1++;
                          if (obg.test1 == Vikar[i].test1)
                          {
                              stat1 --;
                          }
                      }
                   if (ck5 > 1)
                      {
                          stat1++;
                          if (obg.test2 == Vikar[i].test2)
                          {
                              stat1 --;
                          }
                      }

                   if (stat1 == 0)
                   {
                       answ++;

                       obl.log = Vikar[i].log;
                       obl.pas = Vikar[i].pas;
                       obl.status = Vikar[i].status;
                       obl.test1 = Vikar[i].test1;
                       obl.test2 = Vikar[i].test2;
                       sort_db.push_back(obl);

                   }


              }

          }


      if (answ == 0)
      {
      return 0;
      }
      else{
          return 1;

      }

    }


void Find::resirvedText(QString str)
{
    ui->labelSt->setText(str);
}

void Find::on_pushButton_3_clicked()
{
    Client->write("Database");
    ui->FIO->setText("");
    ui->TEL->setText("");
    ui->DATA->setText("");
    ui->lineEdit->setText("");
    ui->lineEdit_2->setText("");
}

void Find::slot_connectSocket(qintptr id)
{
    Client->setSocketDescriptor(id);
    connect(Client, SIGNAL(readyRead()), SLOT(slotReadyReady()));
    Client->write("Database");
    slotReadyReady();
}
