#ifndef CHANGEDATA_H
#define CHANGEDATA_H

#include <QDialog>
#include "QStandardItemModel"
#include <QTcpSocket>
#include "accs.h"

namespace Ui {
class changedata;
}

class changedata : public QDialog
{
    Q_OBJECT
signals:
    void connectSocket(qintptr id);
public:
    explicit changedata(QWidget *parent = nullptr);
    ~changedata();

private slots:
    void on_BAT2_clicked();
    void slot_connectSocket(qintptr id);
    void on_pushButton_clicked();
    void slotReadyReady();

private:
    Ui::changedata *ui;
    QStandardItemModel *BDmodel;
    vector<dataS> Vikar;
    QTcpSocket * Client;
};

#endif // CHANGEDATA_H
