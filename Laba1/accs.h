#ifndef ACCS_H
#define ACCS_H

#include <QDialog>
#include <QCoreApplication>
#include <QVariant>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <iostream>
#include <QMainWindow>
#include <QTcpSocket>
#include "dataS.h"
#include "QStandardItemModel"

namespace Ui {
class Accs;
}

class Accs : public QDialog
{
    Q_OBJECT

public:
    explicit Accs(QWidget *parent = nullptr);
    ~Accs();
signals:
    void connectSocket(qintptr id);

private slots:
    void on_pushButton_clicked();
    void slot_connectSocket(qintptr id);
    void on_pushButton_2_clicked();

    void on_pushButton_5_clicked();
    void slotReadyReady();
private:
    Ui::Accs *ui;
    QTcpSocket * Client;
    QSqlDatabase db;
    vector<dataS> Vika;
    QStandardItemModel *BDmodel;
};

#endif // ACCS_H
