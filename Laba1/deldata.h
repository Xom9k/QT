#ifndef DELDATA_H
#define DELDATA_H

#include <QDialog>
#include "QStandardItemModel"
#include <QTcpSocket>
#include "accs.h"

namespace Ui {
class DelData;
}

class DelData : public QDialog
{
    Q_OBJECT
signals:
    void connectSocket(qintptr id);
public:
    explicit DelData(QWidget *parent = nullptr);
    ~DelData();

private slots:
    void on_BAtton_clicked();
    void slot_connectSocket(qintptr id);
    void on_pushButton_clicked();
    void slotReadyReady();

private:
    Ui::DelData *ui;
    QStandardItemModel *BDmodel;
    vector<dataS> Vikar;
    QTcpSocket * Client;
};

#endif // DELDATA_H
