#include "space.h"
#include "ui_space.h"
#include "admin.h"
#include "accs.h"

Space::Space(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Space)
{
    ui->setupUi(this);
    Client = new QTcpSocket;
}

Space::~Space()
{
    delete ui;
}

void Space::on_pushButton_clicked()
{
    hide();
    Admin window;
    qintptr id = Client->socketDescriptor();
    connect(this, SIGNAL(connectSocket(qintptr)), &window, SLOT(slot_connectSocket(qintptr)));
    emit connectSocket(id);
    window.setModal(true);
    window.exec();
    show();
}

void Space::on_pushButton_2_clicked()
{
    hide();
    Accs window;
    qintptr id = Client->socketDescriptor();
    connect(this, SIGNAL(connectSocket(qintptr)), &window, SLOT(slot_connectSocket(qintptr)));
    emit connectSocket(id);
    window.setModal(true);
    window.exec();
    show();
}


void Space::slot_connectSocket(qintptr id)
{
    Client->setSocketDescriptor(id);
}
