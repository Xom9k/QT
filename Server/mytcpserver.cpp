#include "mytcpserver.h"
#include <QtDebug>
#include <QCoreApplication>
#include <string>
#include <iostream>
#include <fstream>
#include <openssl/conf.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/aes.h>
using namespace std;


mytcpserver::~mytcpserver()
{


    foreach(int i, SClients.keys())
    {
        QTextStream os(SClients[i]);
        SClients[i]->close();
        SClients.remove(i);
    }
   server_status = 0;
    mTcpServer->close();
}

mytcpserver::mytcpserver(QObject *parent) : QObject(parent) {

    mTcpServer = new QTcpServer(this);
    counter = 0;
    obj = new BaseDate;
    connect(mTcpServer, &QTcpServer::newConnection,this, &mytcpserver::slotNewConnection);
    if(!mTcpServer->listen(QHostAddress::Any, 33333))

    {
        qDebug() << "server is not started" ;
    }
    else {
        qDebug() << "ЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕ\nЕЕЕЕЕЕЕ  ЕЕЕЕЕЕЕ\nЕЕЕЕЕЕ    ЕЕЕЕЕЕ\nЕЕЕЕЕ      ЕЕЕЕЕ\nЕЕЕЕ        ЕЕЕЕ\nЕЕЕ          ЕЕЕ\nЕЕ            ЕЕ\nЕ              Е\nЕ              Е\nЕЕ            ЕЕ\nЕЕЕ          ЕЕЕ\nЕЕЕЕ        ЕЕЕЕ\nЕЕЕЕЕ      ЕЕЕЕЕ\nЕЕЕЕЕЕ    ЕЕЕЕЕЕ\nЕЕЕЕЕЕЕ  ЕЕЕЕЕЕЕ\nЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕЕ";
        server_status=1;
 //       obj->addacc("admin","123","admin");
//        obj->addbd("test","test","test","test","test");
    }


}
void mytcpserver::slotNewConnection()
{
    if(server_status==1)
    {
        counter++;
        QTcpSocket *temp = mTcpServer->nextPendingConnection();
        int id = (int)temp->socketDescriptor();
        SClients[id] = temp;
        qDebug() << "Amount of users:" << counter << endl;
        connect(temp, &QTcpSocket::readyRead, this, &mytcpserver::slotServerReadMany);
        connect(temp, &QTcpSocket::disconnected, this, &mytcpserver::slotClientDisconnected);
    }
}

QByteArray dec(unsigned char *res, int crlen) //decryption
{

    unsigned char *key = (unsigned char *) "11111111111111111111111111111111";
    unsigned char *iv = (unsigned char *) "0123456789012345";

     char decrypt[256];

    EVP_CIPHER_CTX *ctx;

    ctx = EVP_CIPHER_CTX_new();
    EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv);
    int len = 0;

    EVP_DecryptUpdate(ctx, (unsigned char *)decrypt, &len, res, crlen);
    EVP_DecryptFinal_ex(ctx, (unsigned char *)decrypt + len, &len);


    cout << "decrypted text:"<< decrypt << endl;

    return  decrypt;
}

void mytcpserver::slotServerReadMany()
{
    QTcpSocket *temp = (QTcpSocket*)sender();

    QByteArray message;
    QByteArray test;

    while(temp->bytesAvailable())
    {
    QByteArray buffer = temp->readAll();
    message.append(buffer);
    }
    qDebug() << message;

    unsigned char  testrr[256];
    memcpy(testrr, message.data(), message.size());
    QByteArray res = dec(testrr,  message.size());




    if (res.indexOf("log_in") >= 0) {
        int begin = res.indexOf("$");
        int mid = res.indexOf("@");
        int end = res.indexOf("&");
        QString log = res.mid((begin + 1), (mid - begin - 1));
        QString pass = res.mid((mid + 1), (end - mid - 1));
        if ( obj->search(log,pass) == true ) {
            QString test = obj->status(log,pass);
            QByteArray test_2;
            test_2.append(test);
            temp->write(test_2);}

    }
    if (res.indexOf("reg") >= 0) {
        int begin = res.indexOf("$");
        int mid = res.indexOf("@");
        int end = res.indexOf("&");
        int supa_end = res.indexOf("#");
        QString log = res.mid((begin + 1), (mid - begin - 1));
        QString pass = res.mid((mid + 1), (end - mid - 1));
        QString status = res.mid((end + 1), (supa_end - end - 1));
        if ( obj->search(log,pass) == true) {
            temp->write("no");}
        if ( obj->search(log,pass) == false ) {
            obj->addacc(log,pass,status);
            temp->write("zbs");}
    }
    if (message.indexOf("delacc") >= 0) {
        int begin = message.indexOf("$");
        int mid = message.indexOf("@");
        int end = message.indexOf("&");
        QString log = message.mid((begin + 1), (mid - begin - 1));
        QString pass = message.mid((mid + 1), (end - mid - 1));
        obj->deleteacc(pass,log);
    }
    if (message.indexOf("changeacc") >= 0) {
        int begin = message.indexOf("$");
        int mid = message.indexOf("@");
        int end = message.indexOf("&");
        int supa_end = message.indexOf("#");
        int supa_supa_end = message.indexOf("*");
        QString log_f = message.mid((begin + 1), (mid - begin - 1));
        QString log_t = message.mid((mid + 1), (end - mid - 1));
        QString pass = message.mid((end + 1), (supa_end - end - 1));
        QString status = message.mid((supa_end + 1), (supa_supa_end - supa_end - 1));
        if (end > 0 and pass > 0 ) {
        obj->changeaccpas(log_f,pass);
        }
        if (supa_supa_end > 0) {
        obj->changeaccstat(log_f,status);
        }
        if (log_t > 0 and end > 0) {
        obj->changeacclog(log_f,log_t);
        }
    }
    if (message == "Accounts") {
        vector<dataS> temp_vec;
        QByteArray tempforvec;
        QString sizes;

        temp_vec = obj->Accounts();
        int sizevec = temp_vec.size();
        sizes = QString::number(sizevec);
        tempforvec.append(sizes);

        for(int i = 0; i < sizevec; i++)
        {
           tempforvec.append("@");
           tempforvec.append(temp_vec[i].log);
           tempforvec.append("&");
           tempforvec.append(temp_vec[i].pas);
           tempforvec.append("*");
           tempforvec.append(temp_vec[i].status);
           tempforvec.append("?");
           temp->write(tempforvec);
           tempforvec.clear();
        }
    }
    if (message == "Database") {
        vector<dataS> temp_supa;
        QByteArray tempforvec;
        QString sizes;

        temp_supa = obj->DataShop();
        int sizevec = temp_supa.size();
        sizes = QString::number(sizevec);
        tempforvec.append(sizes);

        for(int i = 0; i < sizevec; i++)
        {
           tempforvec.append("@");
           tempforvec.append(temp_supa[i].log);
           tempforvec.append("&");
           tempforvec.append(temp_supa[i].pas);
           tempforvec.append("*");
           tempforvec.append(temp_supa[i].status);
           tempforvec.append("?");
           tempforvec.append(temp_supa[i].test1);
           tempforvec.append("!");
           tempforvec.append(temp_supa[i].test2);
           tempforvec.append("^");
           temp->write(tempforvec);
           tempforvec.clear();
        }
    }
    if (message.indexOf("deldb") >= 0 ) {
        int begin = message.indexOf("$");
        int mid = message.indexOf("@");
        QString log_f = message.mid((begin + 1), (mid - begin - 1));
        obj->deletebd(log_f);
    }
    if (message.indexOf("addbd") >= 0) {
        int begin = message.indexOf("$");
        int mid = message.indexOf("@");
        int end = message.indexOf("&");
        int supa_end = message.indexOf("#");
        int supa_supa_end = message.indexOf("*");
        int supa_supa_supa_end = message.indexOf("?");
        QString log_f = message.mid((begin + 1), (mid - begin - 1));
        QString log_t = message.mid((mid + 1), (end - mid - 1));
        QString pass = message.mid((end + 1), (supa_end - end - 1));
        QString status = message.mid((supa_end + 1), (supa_supa_end - supa_end - 1));
        QString test = message.mid((supa_supa_end + 1), (supa_supa_supa_end - supa_supa_end - 1));
        obj->addbd(log_f,log_t,pass,status,test);
    }
    if (message.indexOf("changedb") >= 0 ) {
        int begin = message.indexOf("$");
        int mid = message.indexOf("@");
        int end = message.indexOf("&");
        int supa_end = message.indexOf("#");
        int supa_supa_end = message.indexOf("*");
        int supa_supa_supa_end = message.indexOf("?");
        int supa_supa_supa_endd = message.indexOf("!");
        QString log_f = message.mid((begin + 1), (mid - begin - 1));
        QString log_t = message.mid((mid + 1), (end - mid - 1));
        QString pass = message.mid((end + 1), (supa_end - end - 1));
        QString status = message.mid((supa_end + 1), (supa_supa_end - supa_end - 1));
        QString status1 = message.mid((supa_supa_end + 1), (supa_supa_supa_end - supa_supa_end - 1));
        QString status2 = message.mid((supa_supa_supa_end + 1), (supa_supa_supa_endd - supa_supa_supa_end - 1));
        if (log_t > 0 and end > 0) {
        obj->changedb(log_f,"Tip",log_t);
        }
        if (end > 0 and pass > 0 ) {
        obj->changedb(log_f,"Otkuda",pass);
        }
        if ( supa_end > 0 and supa_supa_end > 0) {
        obj->changedb(log_f,"To4ka",status);
        }
        if (supa_supa_supa_end > 0  and supa_supa_end > 0 ) {
        obj->changedb(log_f,"VVod",status1);
        }
        if (status2 > 0 and supa_supa_supa_endd > 0) {
        obj->changedb(log_f,"Vsvod",status2);
        }
    }
    message.clear();
}



void mytcpserver::slotClientDisconnected()
{

    if(server_status==1){

        QTcpSocket *temp = (QTcpSocket*)sender();
        int id = (int)temp->socketDescriptor();
        counter --;
        qDebug() << "Amount of users:" << counter << endl;

        qDebug() << QString::fromUtf8("User is disconected!");
        temp->write("You have been disconeted");
        temp->close();

    }
}

