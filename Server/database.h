#ifndef BASEDATE_H
#define BASEDATE_H

#endif // BASEDATE_H

#include <QCoreApplication>
#include <QVariant>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <iostream>

using namespace std;


struct dataS
{
    QString log;
    QString pas;
    QString status;
    QString test1;
    QString test2;
};

class BaseDate
{

private:
    QSqlDatabase db;
    vector<dataS> VIKA;


public:
    BaseDate()
    {
     db = QSqlDatabase::addDatabase("QSQLITE");
     db.setDatabaseName("Basedata");
     db.open();
     QSqlQuery query(db);
     query.exec("CREATE TABLE User("
                "login VARCHAR(20) NOT NULL, "
                "password VARCHAR(20) NOT NULL,"
                "status VARCHAR(20) NOT NULL"
                ")"
                );
    query.exec("CREATE TABLE Sweetshoper("
           "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
           "Tip VARCHAR(20) NOT NULL, "
           "Otkuda VARCHAR(20) NOT NULL,"
           "To4ka VARCHAR(20) NOT NULL,"
           "VVod VARCHAR(20) NOT NULL,"
           "Vsvod VARCHAR(20) NOT NULL"
           ")"
            );
    }


void addacc(QString a, QString b,QString c)
{
    QSqlQuery query;

    query.prepare("INSERT INTO User(login, password, status) "
                      "VALUES (:login, :password, :status)");

    query.bindValue(":password", b);
    query.bindValue(":login", a);
    query.bindValue(":status", c);

    query.exec();
}
void addbd(QString b,QString b2,QString b3,QString b4,QString b5)
{
    QSqlQuery query;

    query.prepare("INSERT INTO Sweetshoper(Tip, Otkuda, To4ka, VVod, Vsvod) "
                      "VALUES (:Tip, :Otkuda, :To4ka, :VVod, :Vsvod )");

    query.bindValue(":Tip", b);
    query.bindValue(":Otkuda", b2);
    query.bindValue(":To4ka", b3);
    query.bindValue(":VVod", b4);
    query.bindValue(":Vsvod", b5);


    query.exec();
}





void deleteacc(QString column, QString temp)
{
    QSqlQuery query;
    QString temp1;
    temp1 = "DELETE FROM User WHERE "+ column + " = \"" + temp + "\"";
    query.exec(temp1);

}

void deletebd(QString temp)
{
    QSqlQuery query;
    QString temp1;
    temp1 = "DELETE FROM Sweetshoper WHERE  ID  = \"" + temp + "\"";
    query.exec(temp1);

}

void changeacclog(QString before, QString log_t)
{
    QSqlQuery query;
    QString temp1;

    temp1 = "UPDATE User SET login = \"" +log_t +"\" "  + "WHERE " + "login = \"" + before + "\"";
    query.exec(temp1);

}
void changeaccpas(QString before, QString pas_t)
{
    QSqlQuery query;
    QString temp1;

    temp1 = "UPDATE User SET password = \"" +pas_t +"\" " + "WHERE " + "login = \"" + before + "\"";
    query.exec(temp1);

}
void changeaccstat(QString before, QString status_t)
{
    QSqlQuery query;
    QString temp1;

    temp1 = "UPDATE User SET status = \"" +status_t +"\" "   + "WHERE " + "login = \"" + before + "\"";
    query.exec(temp1);

}
void changedb(QString ID ,QString before, QString log_t)
{
    QSqlQuery query;
    QString temp1;

    temp1 = "UPDATE Sweetshoper SET " + before + " = \"" +log_t +"\" "  + "WHERE ID = \"" + ID + "\"";
    query.exec(temp1);

}
QString status(QString log, QString pas){
    QSqlQuery query;
    QString status;
    QString temp1 = "SELECT * FROM User WHERE login = \"" + log +"\" and password = \"" + pas + "\"";
    query.exec(temp1);
    query.first();
    status = query.value(2).toString();
    if(query.first()){
        return status;
    }
    else {
        return "ERROR";
    }

}

bool search(QString log, QString pas)
{
    QSqlQuery query;
    QString temp1 = "SELECT * FROM User WHERE login = \"" + log +"\" and password = \"" + pas + "\"";

    query.exec(temp1);
    if(query.first())
    {return true;}
    else
    {return false;}





}

    vector<dataS> Accounts()
{
    QSqlQuery query;
    dataS temp;
    vector<dataS> temp_vector;
    query.exec("SELECT * FROM User");
    QSqlRecord rec = query.record();
        const int loginIndex = rec.indexOf("login");//номер "столбца"
        const int passwordIndex = rec.indexOf("password");
        const int statusIndex = rec.indexOf("status");

        while(query.next())
        {
            temp.log = query.value(loginIndex).toString();
            temp.pas = query.value(passwordIndex).toString();
            temp.status = query.value(statusIndex).toString();
            temp_vector.push_back(temp);
        }
        return temp_vector;
}

    vector<dataS> DataShop()
{
    QSqlQuery query;
    dataS temp;
    vector<dataS> temp_vector;
    query.exec("SELECT * FROM Sweetshoper");
    QSqlRecord rec = query.record();
        const int loginIndex = rec.indexOf("Tip");//номер "столбца"
        const int passwordIndex = rec.indexOf("Otkuda");
        const int statusIndex = rec.indexOf("To4ka");
        const int vvodIndex = rec.indexOf("VVod");
        const int vsvodIndex = rec.indexOf("Vsvod");

        while(query.next())
        {
            temp.log = query.value(loginIndex).toString();
            temp.pas = query.value(passwordIndex).toString();
            temp.status = query.value(statusIndex).toString();
            temp.test1 = query.value(vvodIndex).toString();
            temp.test2 = query.value(vsvodIndex).toString();
            temp_vector.push_back(temp);
        }
        return temp_vector;
}
};
