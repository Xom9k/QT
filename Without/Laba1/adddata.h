#ifndef ADDDATA_H
#define ADDDATA_H

#include <QDialog>
#include "QStandardItemModel"

namespace Ui {
class AddData;
}

class AddData : public QDialog
{
    Q_OBJECT

public:
    explicit AddData(QWidget *parent = nullptr);
    ~AddData();

private slots:
    void on_Batton1_clicked();


    void on_pushButton_clicked();

private:
    Ui::AddData *ui;
    QStandardItemModel *BDmodel;
};


#endif


