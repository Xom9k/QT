#include "deldata.h"
#include "ui_deldata.h"
#include "database.h"
#include "admin.h"


DelData::DelData(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DelData)
{
    ui->setupUi(this);
    QPixmap bkgnd("../Laba1/333.jpeg");
        bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
        QPalette palette;
        palette.setBrush(QPalette::Background, bkgnd);
        this->setPalette(palette);

    DataBase obj;
    dataS ob;
    obj.read();
    int k = obj.dbSize();;


    BDmodel = new QStandardItemModel(k,3,this);
    ui->tableView->setModel(BDmodel);
    QModelIndex index;
    QStringList list = {"SURNAME", "TELEPHONE", "DATE"};
    BDmodel->QStandardItemModel::setHorizontalHeaderLabels(list);


    for (int col = 0; col < BDmodel->columnCount(); col++)
    {
        int n = 0;
        for (int row = 0; row < BDmodel->rowCount(); row++)
        {
            index = BDmodel->index(row, col);
             dataS base = obj.pop(n);
            if (col == 0)
            {

                  string value = base.fio;
                  QString qvalue = QString::fromStdString(value);
                  BDmodel->setData(index, qvalue);
            }
            else if (col == 1)
            {

                string value = base.tel;
                QString qvalue = QString::fromStdString(value);
                BDmodel->setData(index, qvalue);

            }
            else
            {
                string value = base.date;
                QString qvalue = QString::fromStdString(value);
                BDmodel->setData(index, qvalue);

            }

            n++;
        }

    }

}

DelData::~DelData()
{
    delete ui;
}

void DelData::on_BAtton_clicked()
{
    QString ids = ui->ID->text();
    int id = ids.toInt();

    DataBase obj;
    obj.del(id);
    hide();


    Admin window1;
    window1.setModal(true);
    window1.exec();
}



void DelData::on_pushButton_clicked()
{
    hide();
    Admin window1;
    window1.setModal(true);
    window1.exec();
}
