#include "changedata.h"
#include "ui_changedata.h"
#include "database.h"
#include "admin.h"

changedata::changedata(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::changedata)
{
    ui->setupUi(this);
    QPixmap bkgnd("../Laba1/222.jpg");
        bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
        QPalette palette;
        palette.setBrush(QPalette::Background, bkgnd);
        this->setPalette(palette);

    DataBase obj;
    dataS ob;
    obj.read();
    int k = obj.dbSize();;


BDmodel = new QStandardItemModel(k,3,this);
    ui->tableView->setModel(BDmodel);
    QModelIndex index;
    QStringList list = {"SURNAME", "TELEPHONE", "DATE"};
    BDmodel->QStandardItemModel::setHorizontalHeaderLabels(list);


    for (int col = 0; col < BDmodel->columnCount(); col++)
    {
        int n = 0;
        for (int row = 0; row < BDmodel->rowCount(); row++)
        {
            index = BDmodel->index(row, col);
             dataS base = obj.pop(n);
            if (col == 0)
            {

                  string value = base.fio;
                  QString qvalue = QString::fromStdString(value);
                  BDmodel->setData(index, qvalue);
            }
            else if (col == 1)
            {

                string value = base.tel;
                QString qvalue = QString::fromStdString(value);
                BDmodel->setData(index, qvalue);

            }
            else
            {
                string value = base.date;
                QString qvalue = QString::fromStdString(value);
                BDmodel->setData(index, qvalue);

            }

            n++;
        }

    }

}


changedata::~changedata()
{
    delete ui;
}

void changedata::on_BAT2_clicked()
{
    QString ids = ui->ID->text();
    QString fio = ui->NAME->text();
    QString tel = ui->TEL->text();
    QString date = ui->DATA->text();

    int id = ids.toInt();
    string fios = fio.toStdString();
    string tels = tel.toStdString();
    string dates = date.toStdString();

    dataS temp;
    temp.fio = fios;
    temp.tel = tels;
    temp.date = dates;
    temp.id = id;
    DataBase obj;
    obj.change(id, temp);
    hide();


    Admin window1;
    window1.setModal(true);
    window1.exec();
}

void changedata::on_pushButton_clicked()
{

        hide();
        Admin window1;
        window1.setModal(true);
        window1.exec();
}
