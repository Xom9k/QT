#include "adddata.h"
#include "ui_adddata.h"
#include "database.h"
#include "admin.h"


AddData::AddData(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddData)
{
    ui->setupUi(this);
    QPixmap bkgnd("../Laba1/111.jpg");
        bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
        QPalette palette;
        palette.setBrush(QPalette::Background, bkgnd);
        this->setPalette(palette);

        DataBase obj;
        dataS ob;
        obj.read();
        int k = obj.dbSize();;


    BDmodel = new QStandardItemModel(k,3,this);
        ui->tableView->setModel(BDmodel);
        QModelIndex index;
        QStringList list = {"SURNAME", "TELEPHONE", "DATE"};
        BDmodel->QStandardItemModel::setHorizontalHeaderLabels(list);


        for (int col = 0; col < BDmodel->columnCount(); col++)
        {
            int n = 0;
            for (int row = 0; row < BDmodel->rowCount(); row++)
            {
                index = BDmodel->index(row, col);
                 dataS base = obj.pop(n);
                if (col == 0)
                {

                      string value = base.fio;
                      QString qvalue = QString::fromStdString(value);
                      BDmodel->setData(index, qvalue);
                }
                else if (col == 1)
                {

                    string value = base.tel;
                    QString qvalue = QString::fromStdString(value);
                    BDmodel->setData(index, qvalue);

                }
                else
                {
                    string value = base.date;
                    QString qvalue = QString::fromStdString(value);
                    BDmodel->setData(index, qvalue);

                }

                n++;
            }

        }

}

AddData::~AddData()
{
    delete ui;
}



void AddData::on_Batton1_clicked()
{
    QString fio = ui->Name->text();
    QString tel = ui->Tel->text();
    QString date = ui->Data->text();

    string fios = fio.toStdString();
    string tels = tel.toStdString();
    string dates = date.toStdString();

    dataS temp;
    temp.fio = fios;
    temp.tel = tels;
    temp.date = dates;

    DataBase obj;
    obj.read();

    obj.add(temp);

    obj.write();

    hide();

    Admin window1;
    window1.setModal(true);
    window1.exec();


}

void AddData::on_pushButton_clicked()
{
    hide();
    Admin window1;
    window1.setModal(true);
    window1.exec();
}
