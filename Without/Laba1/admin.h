#ifndef ADMIN_H
#define ADMIN_H

#include <QDialog>
#include "QStandardItemModel"


namespace Ui {
class Admin;
}

class Admin : public QDialog
{
    Q_OBJECT

public:
    explicit Admin(QWidget *parent = nullptr);
    ~Admin();

public slots:



private slots:

        void on_Batton1_clicked();

        void on_Batton2_clicked();

        void on_Batton3_clicked();


        void on_pushButton_clicked();

private:
    Ui::Admin *ui;
    QStandardItemModel *BDmodel;
};

#endif // ADMIN_H
