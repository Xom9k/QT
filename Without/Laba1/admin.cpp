#include "admin.h"
#include "ui_admin.h"
#include "adddata.h"
#include "changedata.h"
#include "deldata.h"
#include "database.h"
#include "find.h"

Admin::Admin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Admin)
{
    ui->setupUi(this);
    QPixmap bkgnd("../Laba1/123.jpg");
        bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
        QPalette palette;
        palette.setBrush(QPalette::Background, bkgnd);
        this->setPalette(palette);

        DataBase obj;
        dataS ob;
        obj.read();
        int k = obj.dbSize();;


    BDmodel = new QStandardItemModel(k,3,this);
        ui->tableView->setModel(BDmodel);
        QModelIndex index;
        QStringList list = {"SURNAME", "TELEPHONE", "DATE"};
        BDmodel->QStandardItemModel::setHorizontalHeaderLabels(list);


        for (int col = 0; col < BDmodel->columnCount(); col++)
        {
            int n = 0;
            for (int row = 0; row < BDmodel->rowCount(); row++)
            {
                index = BDmodel->index(row, col);
                 dataS base = obj.pop(n);
                if (col == 0)
                {

                      string value = base.fio;
                      QString qvalue = QString::fromStdString(value);
                      BDmodel->setData(index, qvalue);
                }
                else if (col == 1)
                {

                    string value = base.tel;
                    QString qvalue = QString::fromStdString(value);
                    BDmodel->setData(index, qvalue);

                }
                else
                {
                    string value = base.date;
                    QString qvalue = QString::fromStdString(value);
                    BDmodel->setData(index, qvalue);

                }

                n++;
            }

        }

}

Admin::~Admin()
{
    delete ui;
}

DataBase obj;

void Admin::on_Batton1_clicked()
{
    close();
    AddData window1;
    window1.setModal(true);
    window1.exec();
    show();
    obj.upload();
}

void Admin::on_Batton2_clicked()
{
    close();
    changedata window2;
    window2.setModal(true);
    window2.exec();
    show();
    obj.upload();
}

void Admin::on_Batton3_clicked()
{
    close();
    DelData window3;
    window3.setModal(true);
    window3.exec();
    show();
    obj.upload();
}




void Admin::on_pushButton_clicked()
{
    close();
    Find window3;
    window3.setModal(true);
    window3.exec();
    show();
    obj.upload();
}
