#ifndef DELDATA_H
#define DELDATA_H

#include <QDialog>
#include "QStandardItemModel"


namespace Ui {
class DelData;
}

class DelData : public QDialog
{
    Q_OBJECT

public:
    explicit DelData(QWidget *parent = nullptr);
    ~DelData();

private slots:
    void on_BAtton_clicked();

    void on_pushButton_clicked();

private:
    Ui::DelData *ui;
    QStandardItemModel *BDmodel;
};

#endif // DELDATA_H
