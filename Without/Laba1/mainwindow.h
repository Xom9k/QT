#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_Batton_clicked();
    void slotReadyRead();
    void on_pushButton_clicked();
    void slotSendToServer();
private:
    Ui::MainWindow *ui;
    QTcpSocket * Client;
};

#endif // MAINWINDOW_H
