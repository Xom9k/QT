#ifndef FIND_H
#define FIND_H

#include <QDialog>
#include "QStandardItemModel"

namespace Ui {
class Find;
}

class Find : public QDialog
{
    Q_OBJECT

public:
    explicit Find(QWidget *parent = nullptr);
    ~Find();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::Find *ui;
    QStandardItemModel *BDmodel;
};

#endif // FIND_H
