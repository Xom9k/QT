#include "find.h"
#include "ui_find.h"
#include "admin.h"
#include "database.h"

Find::Find(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Find)
{
    ui->setupUi(this);
    DataBase obj;
            dataS ob;
            obj.read();
            int k = obj.dbSize();


        BDmodel = new QStandardItemModel(k,3,this);
            ui->tableView->setModel(BDmodel);
            QModelIndex index;
            QStringList list = {"SURNAME", "TELEPHONE", "DATE"};
            BDmodel->QStandardItemModel::setHorizontalHeaderLabels(list);


            for (int col = 0; col < BDmodel->columnCount(); col++)
            {
                int n = 0;
                for (int row = 0; row < BDmodel->rowCount(); row++)
                {
                    index = BDmodel->index(row, col);
                     dataS base = obj.pop(n);
                    if (col == 0)
                    {

                          string value = base.fio;
                          QString qvalue = QString::fromStdString(value);
                          BDmodel->setData(index, qvalue);
                    }
                    else if (col == 1)
                    {

                        string value = base.tel;
                        QString qvalue = QString::fromStdString(value);
                        BDmodel->setData(index, qvalue);

                    }
                    else
                    {
                        string value = base.date;
                        QString qvalue = QString::fromStdString(value);
                        BDmodel->setData(index, qvalue);

                    }

                    n++;
                }

            }




    }



Find::~Find()
{
    delete ui;
}

void Find::on_pushButton_2_clicked()
{
    hide();
    Admin window1;
    window1.setModal(true);
    window1.exec();
}

void Find::on_pushButton_clicked()
{
    DataBase obj;
    dataS ob;
    obj.read();

    QString fio  = ui->FIO->text();
    QString tel  = ui->TEL->text();
    QString date = ui->DATA->text();

    string rightDate = date.toStdString();
    string rightFio = fio.toStdString();
    string rightTel = tel.toStdString();

    ob.fio = rightFio;
    ob.date = rightDate;
    ob.tel = rightTel;

    int a1 = rightFio.length();
    int a2 = rightTel.length();
    int a3 = rightDate.length();

    if (obj.sort(ob, a1, a2, a3) == 1)

    {
          int k = obj.SortdbSize();
          BDmodel = new QStandardItemModel(k,3,this);
          ui->tableView->setModel(BDmodel);
          QStringList list = {"SURNAME", "TELEPHONE", "DATE"};
          BDmodel->QStandardItemModel::setHorizontalHeaderLabels(list);

          QModelIndex index;



          for (int col = 0; col < BDmodel->columnCount(); col++)
          {
              int n = 0;
              for (int row = 0; row < BDmodel->rowCount(); row++)
              {
                  index = BDmodel->index(row, col);
                   dataS base = obj.sortpop(n);
                  if (col == 0)
                  {

                        string value = base.fio;
                        QString qvalue = QString::fromStdString(value);
                        BDmodel->setData(index, qvalue);
                  }
                  else if (col == 1)
                  {

                      string value = base.tel;
                      QString qvalue = QString::fromStdString(value);
                      BDmodel->setData(index, qvalue);

                  }
                  else
                  {
                      string value = base.date;
                      QString qvalue = QString::fromStdString(value);
                      BDmodel->setData(index, qvalue);

                  }

                  n++;
              }

           }




}
}
