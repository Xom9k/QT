#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"
#include <cstring>
#include <string>
#include <fstream>
#include <iostream>
#include "admin.h"
#include "user.h"
#include "manager.h"
#include "reg.h"
using namespace std;



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    Client = new QTcpSocket;
    Client->connectToHost("127.0.0.1", 33333);
    connect(Client, SIGNAL(readyRead()), SLOT(slotReadyRead()));
    ui->setupUi(this);
    QPixmap bkgnd("../Laba1/asd.jpg");
        bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
        QPalette palette;
        palette.setBrush(QPalette::Background, bkgnd);
        this->setPalette(palette);
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::slotReadyRead(){

    QString message;
    QMessageBox msgBox;


    while(Client->bytesAvailable())
    {
    QString buffer = Client->readAll();
    message.append(buffer);
    }

    if (message == "admin")
       {
            hide();
            Admin window;
            window.setModal(true);
            window.exec();
            ui->vvodLog->setText("");
            ui->vvodPass->setText("");
            show();


       }
       else if(message == "user")
       {
           hide();
           User window;
           window.setModal(true);
           window.exec();
           ui->vvodLog->setText("");
           ui->vvodPass->setText("");
           show();

       }

       else if(message == "manager")
       {
          hide();
          Manager window;
          window.setModal(true);
          window.exec();
          ui->vvodLog->setText("");
          ui->vvodPass->setText("");
          show();
       }

       else {

           msgBox.setText("Login is incorrect");
           msgBox.setInformativeText("Do you want to retry?");
           msgBox.setStandardButtons(QMessageBox::Retry | QMessageBox::Close);
           msgBox.setDefaultButton(QMessageBox :: Retry);
           ui->vvodLog->setText("");
           ui->vvodPass->setText("");

       }
}


void MainWindow::slotSendToServer(){
    QString login  = ui->vvodLog->text();
    QString pas  = ui->vvodPass->text();
    QByteArray temp;
    QString tempo = login + " " + pas;
    temp.append(tempo);
    Client->write(temp);
}


void MainWindow::on_Batton_clicked()
{
    slotSendToServer();
    slotReadyRead();
}


void MainWindow::on_pushButton_clicked()
{
    hide();
    reg  window;
    window.setModal(true);
    window.exec();
    show();
}
