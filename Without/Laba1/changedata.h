#ifndef CHANGEDATA_H
#define CHANGEDATA_H

#include <QDialog>
#include "QStandardItemModel"

namespace Ui {
class changedata;
}

class changedata : public QDialog
{
    Q_OBJECT

public:
    explicit changedata(QWidget *parent = nullptr);
    ~changedata();

private slots:
    void on_BAT2_clicked();

    void on_pushButton_clicked();

private:
    Ui::changedata *ui;
    QStandardItemModel *BDmodel;
};

#endif // CHANGEDATA_H
